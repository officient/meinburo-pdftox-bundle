<?php

namespace Officient\MeinburoPdfToX;

use Officient\MeinburoPdfToX\Exception\MeinburoPdfToXException;

interface ClientInterface
{
    public const METHOD_GET = 'GET';
    public const METHOD_POST = 'POST';
    public const METHOD_PATCH = 'PATCH';
    public const METHOD_PUT = 'PUT';
    public const METHOD_DELETE = 'DELETE';

    /**
     * @param string $query
     * @param null $data
     * @param string $method
     * @return Response
     * @throws MeinburoPdfToXException
     */
    public function doRequest(string $query, $data = null, string $method = self::METHOD_GET): Response;
}