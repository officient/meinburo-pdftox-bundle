<?php

namespace Officient\MeinburoPdfToX;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class MeinburoPdfToXBundle
 *
 * This class just needs to be here, because of stuff
 * in symfony documentation...
 *
 * @package Officient\MeinburoPdfToX
 */
class MeinburoPdfToXBundle extends Bundle
{
    //
}