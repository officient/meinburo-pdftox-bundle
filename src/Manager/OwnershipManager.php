<?php

namespace Officient\MeinburoPdfToX\Manager;

use Officient\MeinburoPdfToX\Client;
use Officient\MeinburoPdfToX\Entity\Ownership;

class OwnershipManager extends AbstractManager implements OwnershipManagerInterface
{
    /**
     * @inheritDoc
     */
    public function findOneByOwner(string $ownerPrefix, string $ownerIdent): ?Ownership
    {
        $response = $this->client->doRequest('/ownerships/'.$ownerPrefix.'/'.$ownerIdent);
        if($response->getHttpCode() === 200 && is_array($response->getContent()) && isset($response->getContent()['id'])) {
            $data = $response->getContent();
            $updatedDatetime = \DateTime::createFromFormat(
                'Y-m-d H:i:s.u',
                $data['updatedDatetime']['date'],
                new \DateTimeZone($data['updatedDatetime']['timezone'])
            );
            $createdDatetime = \DateTime::createFromFormat(
                'Y-m-d H:i:s.u',
                $data['createdDatetime']['date'],
                new \DateTimeZone($data['createdDatetime']['timezone'])
            );
            return (new Ownership())
                ->setId($data['id'])
                ->setOwnerPrefix($data['ownerPrefix'])
                ->setOwnerIdent($data['ownerIdent'])
                ->setValue($data['value'])
                ->setUpdatedDatetime($updatedDatetime)
                ->setCreatedDatetime($createdDatetime);
        } else {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function store(string $ownerPrefix, int $ownerIdent, string $value): bool
    {
        $response = $this->client->doRequest('/ownerships', [
            'ownerPrefix' => $ownerPrefix,
            'ownerIdent' => $ownerIdent,
            'value' => $value
        ], Client::METHOD_POST);

        return in_array($response->getHttpCode(), [200, 201]);
    }
}