<?php

namespace Officient\MeinburoPdfToX\Manager;

use Officient\MeinburoPdfToX\ClientInterface;
use Symfony\Component\Serializer\SerializerInterface;

abstract class AbstractManager
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @param ClientInterface $client
     * @param SerializerInterface $serializer
     */
    public function __construct(ClientInterface $client, SerializerInterface $serializer)
    {
        $this->client = $client;
        $this->serializer = $serializer;
    }
}