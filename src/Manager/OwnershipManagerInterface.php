<?php

namespace Officient\MeinburoPdfToX\Manager;

use Officient\MeinburoPdfToX\Entity\Ownership;

interface OwnershipManagerInterface
{
    /**
     * @param string $ownerPrefix
     * @param string $ownerIdent
     * @return Ownership|null
     */
    public function findOneByOwner(string $ownerPrefix, string $ownerIdent): ?Ownership;

    /**
     * @param string $ownerPrefix
     * @param int $ownerIdent
     * @param string $value
     * @return bool
     */
    public function store(string $ownerPrefix, int $ownerIdent, string $value): bool;
}