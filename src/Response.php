<?php

namespace Officient\MeinburoPdfToX;

class Response
{
    /**
     * @var int
     */
    private $httpCode;

    /**
     * @var mixed
     */
    private $content;

    /**
     * @var array
     */
    private $headers;

    /**
     * @param int $httpCode
     * @param mixed $content
     * @param array|null $headers
     */
    public function __construct(int $httpCode, $content, ?array $headers = array())
    {
        $this->httpCode = $httpCode;
        $this->content = $content;
        $this->headers = $headers;
    }

    /**
     * @return int
     */
    public function getHttpCode(): int
    {
        return $this->httpCode;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return array
     */
    public function getHeaders(): ?array
    {
        return $this->headers;
    }
}