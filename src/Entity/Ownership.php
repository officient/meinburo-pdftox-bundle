<?php

namespace Officient\MeinburoPdfToX\Entity;

class Ownership
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string|null
     */
    private $ownerPrefix;

    /**
     * @var int|null
     */
    private $ownerIdent;

    /**
     * @var string|null
     */
    private $value;

    /**
     * @var \DateTimeInterface|null
     */
    private $updatedDatetime;

    /**
     * @var \DateTimeInterface|null
     */
    private $createdDatetime;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Ownership
     */
    public function setId(?int $id): Ownership
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOwnerPrefix(): ?string
    {
        return $this->ownerPrefix;
    }

    /**
     * @param string|null $ownerPrefix
     * @return Ownership
     */
    public function setOwnerPrefix(?string $ownerPrefix): Ownership
    {
        $this->ownerPrefix = $ownerPrefix;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getOwnerIdent(): ?int
    {
        return $this->ownerIdent;
    }

    /**
     * @param int|null $ownerIdent
     * @return Ownership
     */
    public function setOwnerIdent(?int $ownerIdent): Ownership
    {
        $this->ownerIdent = $ownerIdent;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     * @return Ownership
     */
    public function setValue(?string $value): Ownership
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedDatetime(): ?\DateTimeInterface
    {
        return $this->updatedDatetime;
    }

    /**
     * @param \DateTimeInterface|null $updatedDatetime
     * @return Ownership
     */
    public function setUpdatedDatetime(?\DateTimeInterface $updatedDatetime): Ownership
    {
        $this->updatedDatetime = $updatedDatetime;
        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedDatetime(): ?\DateTimeInterface
    {
        return $this->createdDatetime;
    }

    /**
     * @param \DateTimeInterface|null $createdDatetime
     * @return Ownership
     */
    public function setCreatedDatetime(?\DateTimeInterface $createdDatetime): Ownership
    {
        $this->createdDatetime = $createdDatetime;
        return $this;
    }
}